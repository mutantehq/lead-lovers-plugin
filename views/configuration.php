<div class="wrap">
	<h1>Configurações Lead Lovers</h1>
	<small>configurações para adicinar captura lead lovers após o conteudo.</small>

	<form  action="<?php current_url() ?>" method="post">
		<input type="hidden" name="action" value="add_lead">
		<table class="form-table">
			<tr>
				<th>
					STATUS
				</th>
				<td>
					<input type="checkbox" <?php if($data->status === "on"): ?> checked <?php endif; ?> name="status" value="on">
				</td>
			</tr>
			<tr>
				<th>
					ID
				</th>
				<td>
					<input type="text" name="id_lead" value="<?php echo $data->id_lead; ?>" class="regular-text" required="">
				</td>
			</tr>
			<tr>
				<th>
					PID
				</th>
				<td>
					<input type="text" name="pid" value="<?php echo $data->pid; ?>" class="regular-text" required="">
				</td>
			</tr>
			<tr>
				<th>
					LIST
				</th>
				<td>
					<input type="text" name="list" value="<?php echo $data->list; ?>" class="regular-text" required="">
				</td>
			</tr>
			<tr>
				<th>
					PROVIDER
				</th>
				<td>
					<input type="text" name="provider" value="<?php echo $data->provider; ?>" class="regular-text" required="">
				</td>
			</tr>
			<tr>
				<th>
					REDIRECT
				</th>
				<td>
					<input type="text" name="redirect" value="<?php echo $data->redirect; ?>" class="regular-text" required="">
				</td>
			</tr>
			<tr>
				<th>
					TEXTO
				</th>
				<td>
					<input type="text" name="texto" value="<?php echo $data->texto; ?>" class="regular-text" required="">
				</td>
			</tr>
			<tr>
				<th>
				</th>
				<td>
					<button type="submit" name="button" class="button button-primary">ENVIAR</button>

				</td>
			</tr>
		</table>
	</form>
</div>
