<?php
/**
 * Plugin Name: Lead Lovers Content
 * Description: Plugin para Adicionar captura no conteudo do single-post
 * Author: Waldson Vital dos Santos
 */

add_action("admin_menu", "add_admin_menu");
function add_admin_menu(){
	add_options_page("Lead Lovers", "Lead Lovers", "manage_options","lead-lovers","show_config_page_lead");
}



function show_config_page_lead(){
	global $wpdb;
	$ll_name_table = $wpdb->prefix."config_lead";

	$data = $wpdb->get_row("SELECT * FROM $ll_name_table");

	if(isset($_POST["action"]) && $_POST["action"] == "add_lead"){
		$post = $_POST;
		unset($post["action"]);
		unset($post["button"]);

		if(!isset($post["status"])){
			$post['status'] = "off";
		}

		$wpdb->update($ll_name_table, $post, array('id' => $data->id, ));
		echo "<div class='alert'>
			Registro atualizado com sucesso;
		</div>";
		$data = $wpdb->get_row("SELECT * FROM $ll_name_table");
	}

	include "views/configuration.php";
}


function initialze_lead(){
	global $wpdb;
	$ll_name_table = $wpdb->prefix."config_lead";
	$charset = $wpdb->get_charset_collate();

	if($wpdb->get_var("SHOW TABLES LIKE '".$ll_name_table."'") != $ll_name_table){

		$sql = "CREATE TABLE ".$ll_name_table."(
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			id_lead varchar(20),
			pid varchar(20),
			list varchar(20),
			provider varchar(30),
			redirect varchar(255),
			status varchar(9),
			texto text,
			UNIQUE KEY id (id)
		) ".$charset.";";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}

	$insert = array(
		"status" => "off"
	);

	$wpdb->insert($ll_name_table, $insert );
}
// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'initialze_lead');


function current_url(){
	return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

add_action("the_content", "adicionar_form");
function adicionar_form($the_content){
	global $wpdb;
	$ll_name_table = $wpdb->prefix."config_lead";
	$data = $wpdb->get_row("SELECT * FROM $ll_name_table");

	$form = '
		<form action="http://machine.leadlovers.com/Pages/Index/'.$data->id_lead.'" method="post" class="form-lead_theme-1">
			<h3 class="lead-title">'.$data->texto.'</h3>
			<input id="id" name="id" type="hidden" value="'.$data->id_lead.'" />
			<input id="pid" name="pid" type="hidden" value="'.$data->pid.'" />
			<input id="list_id" name="list_id" type="hidden" value="'.$data->list.'" />
			<input id="provider" name="provider" type="hidden" value="'.$data->provider.'" />
			<label for="email">E-mail:</label>
			<input class="form-control" id="email" name="email" placeholder="Informe o seu email" type="text" />
			<label for="name">Nome:</label>
			<input class="form-control" id="name" name="name" placeholder="Informe o seu nome" type="text" />
			<button class="btn btn-danger" style="margin-bottom:5px;" type="submit">Assinar</button>
			<input type="hidden" id="source" name="source" value="" />
			<img src="'.$data->redirect.'" style="display: none;" />
		</form>
		';

	if(is_single()){
		if($data->status === "on"){
			$the_content = $the_content.$form;
		}
	}

	return $the_content;
}


function wpdocs_theme_name_scripts() {
    wp_enqueue_style( 'form-lead_theme-1', plugins_url("themes/form-lead_theme-1.css", __FILE__) );
    // wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
